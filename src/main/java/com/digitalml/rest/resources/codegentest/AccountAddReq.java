package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for AccountAddReq:
{
  "required": [
    "Newelement"
  ],
  "type": "object",
  "properties": {
    "Newelement": {
      "$ref": "Address"
    }
  }
}
*/

public class AccountAddReq {

	@Size(max=1)
	@NotNull
	private com.digitalml.www.information.model.retail.Address newelement;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    address = new com.digitalml.www.information.model.retail.Address();
	}
	public com.digitalml.www.information.model.retail.Address getNewelement() {
		return newelement;
	}
	
	public void setNewelement(com.digitalml.www.information.model.retail.Address newelement) {
		this.newelement = newelement;
	}
}